<?php

// Define app routes

use Slim\App;

return function (App $app) {

    $app->post('/event-in', \App\Action\Event\EventCreateAction::class)->setName('event-in');
    $app->get('/event-list', \App\Action\Event\EventListAction::class)->setName('events-list');
    $app->post('/event-list-json', \App\Action\Event\EventListDataTableAction::class)->setName('event-list');

};
