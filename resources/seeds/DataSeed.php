<?php

use Phinx\Seed\AbstractSeed;

class DataSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'uid'    => '1',
                'client_name' => 'Meshh',
                'event_name' => 'Christmas Party',
                'event_date' => '2020-01-31',
            ],
        ];

        $eventsTable = $this->table('events');
        $eventsTable->insert($data)->save();
    }
}
