<h3 align="center">Sample test for PHP / TD</h3>

## Requirements

Meshh requires a basic web interface that will display the 10 most recently created events on the system. The data will be updated from a separate website, so will require a basic API service that accepts JSON objects via AJAX. The system will be used by up to 100 clients on a regular basis (5-10 times a day each) and must have 99.99% uptime. The API service will need to accept a JSON object with three parameters. It should exist on a URL accessible via HTTP/S and return with success only if the parameters are valid and the data has been stored correctly - otherwise it should return an error response.

The JSON object will have the following parameters:

    clientName (required, string)
    eventName (optional, string)
    date (required, format YYYY-MM-DD)

Task

Create a prototype for the above platform, providing a web accessible URL to display the events and another to collect the data. Provide your code on Github or Bitbucket for review. Document your thoughts on a suitable architecture, possible security requirements / data validation approach, and how this could be extended to allow clients to update their own events. This must be written on either a LAMP or LEMP stack.

A sample valid JSON object is below:

{
    "clientName": "Meshh",
    "eventName": "Christmas Party",
    "date": "2020-01-31"
}

## interpretations

* instruction says "display the 10 most recently created events", not to order them by the supplied date and so the system displays the 10 most recently created with the option to view mor as required.

## setup
composer install
npm i
copy env.example.php to env.php - update with correct db info
* @todo seperate to .env file on root for easier deployment on forge

## Features

/event-list
* displays 10 entries with option to view more/

/event-in
* Receives json object, validates and stores
* Validation Assumes, min - max chars for clientName, eventName, 0 - 100, 0 - 200 respectively, regexp on strings. Date validated with Dateformat + regexp on the string.

/event-list-json
* Handles the DataTables

## Tests

* composer test
* npm run test

This project is build on Slim 4 Skeleton, **[Documentation](https://odan.github.io/slim4-skeleton)**

## License

The MIT License (MIT). Please see [License File](LICENSE) for more information.
