<?php

namespace App\Test\TestCase\Domain\Event\Repository;

use App\Domain\Event\Data\EventCreatorData;
use App\Domain\Event\Repository\EventGeneratorRepository;
use App\Test\Fixture\EventFixture;
use App\Test\TestCase\DatabaseTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Tests.
 */
class EventCreatorRepositoryTest extends TestCase
{
    use DatabaseTestTrait;

    /**
     * Fixtures.
     *
     * @var array
     */
    public $fixtures = [
        EventFixture::class,
    ];

    /**
     * Create instance.
     *
     * @return EventGeneratorRepository The instance
     */
    protected function createInstance(): EventGeneratorRepository
    {
        return $this->getContainer()->get(EventGeneratorRepository::class);
    }

    /**
     * Test.
     *
     * @return void
     */
    public function testInsertEvent(): void
    {
        $repository = $this->createInstance();


        $event = new EventCreatorData();
        $event->clientName = 'Meshh';
        $event->eventName = 'Christmas Party';
        $event->eventDate = '2020-01-31';

        $insertId = $repository->insertEvent($event);

        static::assertIsInt($insertId);

    }
}
