<?php

namespace App\Test\TestCase\Action\Events;

use App\Test\TestCase\HttpTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Test.
 */
final class EventCreateActionTest extends TestCase
{

    use HttpTestTrait;

    /**
     * @throws \Exception
     *
     */
    public function testAction(): void
    {
        $request  = $this->createRequest('POST', '/event-in');
        $request  = $this->withJson($request, ['clientName' => 'Meshh', 'eventName' => 'Christmas Party', 'date'=>'2020-01-31']);
        $response = $this->request($request);

        static::assertSame('{"status":"success"}', (string)$response->getBody());
        static::assertSame('application/json', $response->getHeaderLine('Content-Type'));
        static::assertSame(200, $response->getStatusCode());

        # test malformed date
        $request  = $this->createRequest('POST', '/event-in');
        $request  = $this->withJson($request, ['clientName' => 'Meshh', 'eventName' => 'Christmas Party', 'date'=>'20202-01-31']);
        $response = $this->request($request);

        static::assertSame(500, $response->getStatusCode());

    }


}
