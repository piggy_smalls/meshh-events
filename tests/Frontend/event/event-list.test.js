const EventList = require('../../../templates/event/event-list').EventList;

test('event-list', () => {
    document.body.innerHTML = '<button id="testbutton" type="button">Test Button</button>';

    const event = new EventList();
    const actual = event.demo();

    expect(actual).toBe('Test Button');
});



