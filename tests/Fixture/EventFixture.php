<?php

namespace App\Test\Fixture;

/**
 * Fixture.
 */
class EventFixture
{
    /** @var string Table name */
    public $table = 'events';

    /**
     * Records.
     *
     * @var array Records
     */
    public $records = [
        [
            'uid' => 1,
            'client_name' => 'Meshh',
            'event_name' => 'Christmas Party',
            'event_date' => '2020-01-31'
        ]
    ];
}
