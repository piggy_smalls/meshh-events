<?php

namespace App\Action\Event;

use App\Domain\Event\Data\EventCreatorData;
use App\Domain\Event\Service\EventCreator;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

final class EventCreateAction{

    private $eventCreator;

    public function __construct(EventCreator $eventCreator)
    {
        $this->eventCreator = $eventCreator;
    }

    public function __invoke(ServerRequest $request, Response $response): ResponseInterface
    {

        # get json input
        $event = new EventCreatorData((array)$request->getParsedBody());

        # Invoke the Domain with inputs and retain the result
        $eventId = $this->eventCreator->createEvent($event);

        # if a database record has not been saved and an exception wasn't thrown, give a response
        if(!$eventId){
            return $response
                ->withStatus(500)
                ->withJson([
                'status' => 'there was an issue updating the database, please try again later',
            ]);
        } else {
            return $response
                ->withStatus(200)
                ->withJson([
                'status' => 'success',
            ]);
        }

    }


}

