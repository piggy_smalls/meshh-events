<?php

namespace App\Action\Event;

use App\Domain\Event\Service\EventListDataTable;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

/**
 * Action.
 */
final class EventListDataTableAction
{
    /**
     * @var EventListDataTable
     */
    private $eventListDataTable;

    /**
     * Constructor.
     *
     * @param EventListDataTable $eventListDataTable The service
     */
    public function __construct(EventListDataTable $eventListDataTable)
    {
        $this->eventListDataTable = $eventListDataTable;
    }

    /**
     * Action.
     *
     * @param ServerRequest $request The request
     * @param Response $response The response
     *
     * @return Response The response
     */
    public function __invoke(ServerRequest $request, Response $response): Response
    {
        $params = (array)$request->getParsedBody();
        return $response->withJson($this->eventListDataTable->listAllEvents($params));
    }
}
