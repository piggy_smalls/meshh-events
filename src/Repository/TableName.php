<?php

namespace App\Repository;

/**
 * Table constants.
 */
final class TableName
{
    public const EVENTS = 'events';
}
