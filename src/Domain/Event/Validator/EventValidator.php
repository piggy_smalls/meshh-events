<?php

namespace App\Domain\Event\Validator;

use App\Domain\Event\Data\EventCreatorData;
use Selective\Validation\ValidationResult;

/**
 * Validator.
 */
final class EventValidator
{
    /**
     * Validate.
     *
     * @param EventCreatorData $event The event
     *
     * @return ValidationResult The validation result
     */
    public function validateEvent(EventCreatorData $event): ValidationResult
    {
        $validation = new ValidationResult();

        if (empty($this->sanitizeInput($event->clientName, 2, 100))) {
            $validation->addError('clientName', __('String required - strlen 2, 100'));
        }

        if (empty($this->sanitizeInput($event->eventName, 2, 200))) {
            $validation->addError('eventName', __('String required - strlen 2, 200'));
        }

        // sanitize, instantiate date object to ensure valid date has been supplied
        $eventDate = $this->sanitizeInput($event->eventDate, 10, 10);
        $date = \DateTime::createFromFormat('Y-m-d', $eventDate);

        if (empty($eventDate) || !$date) {
            $validation->addError('eventDate', __('Valid Date required - XXXX-XX-XX'));
        }

        return $validation;
    }

    /**
     * @param string $string
     * @param int $min
     * @param int $max
     * @return string
     * allow a-z,A-Z,0-9 and -
     */
    private function sanitizeInput(string $string, int $min, int $max): string{
        $string = preg_replace("/[^-a-zA-Z0-9_ ]/", "", $string);
        $len = strlen($string);
        if((($min != '') && ($len < $min)) || (($max != '') && ($len > $max)))
            return FALSE;
        return $string;
    }

}
