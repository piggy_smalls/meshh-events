<?php

namespace App\Domain\Event\Data;

use App\Interfaces\DataInterface;
use Selective\ArrayReader\ArrayReader;

final class EventCreatorData implements DataInterface
{

    /** @var int|null */
    public $uid;

    /** @var string */
    public $clientName;

    /** @var string */
    public $eventName;

    /** @var string */
    public $eventDate;

    /**
     * The constructor.
     *
     * @param array $array The array with data
     */
    public function __construct(array $array = [])
    {
        $data = new ArrayReader($array);

        $this->uid = $data->findInt('id');
        $this->clientName = $data->findString('clientName');
        $this->eventName  = $data->findString('eventName');
        $this->eventDate  = $data->findString('date');

    }



}
