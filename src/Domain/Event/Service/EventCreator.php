<?php

namespace App\Domain\Event\Service;

use App\Domain\Event\Data\EventCreatorData;
use App\Domain\Event\Repository\EventGeneratorRepository;
use App\Domain\Event\Validator\EventValidator;
use App\Factory\LoggerFactory;
use App\Interfaces\ServiceInterface;
use Psr\Log\LoggerInterface;
use Selective\Validation\Exception\ValidationException;

/**
 * Domain Service.
 */
final class EventCreator implements ServiceInterface
{
    /**
     * @var EventGeneratorRepository
     */
    private $repository;

    /**
     * @var EventValidator
     */
    protected $eventValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param EventGeneratorRepository $repository The repository
     * @param EventValidator $eventValidator The event validator
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(
        EventGeneratorRepository $repository,
        EventValidator $eventValidator,
        LoggerFactory $loggerFactory
    ) {
        $this->repository = $repository;
        $this->eventValidator = $eventValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('event_creator.log')
            ->createInstance('event_creator');
    }

    /**
     * Create a new event.
     *
     * @param EventCreatorData $event The event data
     *
     * @throws ValidationException
     *
     * @return int The new event ID
     */
    public function createEvent(EventCreatorData $event): int
    {
        // Validation
        $validation = $this->eventValidator->validateEvent($event);

        if ($validation->isFailed()) {
            $validation->setMessage(__('Please check your input'));
            $validation->setCode(400);
            throw new ValidationException($validation);
        }

        // Insert event
        $eventId = $this->repository->insertEvent($event);

        // Logging
        $this->logger->info(__('Event created successfully: %s', $eventId));

        return $eventId;
    }
}
