<?php

namespace App\Domain\Event\Service;

use App\Domain\Event\Repository\EventListDataTableRepository;
use App\Interfaces\ServiceInterface;

/**
 * Service.
 */
final class EventListDataTable implements ServiceInterface
{
    /**
     * @var EventListDataTableRepository
     */
    private $repository;

    /**
     * Constructor.
     *
     * @param EventListDataTableRepository $repository The repository
     */
    public function __construct(EventListDataTableRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * List all events.
     *
     * @param array $params The parameters
     *
     * @return array The result
     */
    public function listAllEvents(array $params): array
    {
        return $this->repository->getTableData($params);
    }
}
