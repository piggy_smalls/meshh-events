<?php

namespace App\Domain\Event\Repository;

use App\Domain\Event\Data\EventCreatorData;
use App\Repository\QueryFactory;
use App\Repository\RepositoryInterface;
use App\Repository\TableName;

/**
 * Repository.
 */
class EventGeneratorRepository implements RepositoryInterface
{
    /**
     * @var QueryFactory The query factory
     */
    private $queryFactory;

    /**
     * Constructor.
     *
     * @param QueryFactory $queryFactory The query factory
     */
    public function __construct(QueryFactory $queryFactory)
    {
        $this->queryFactory = $queryFactory;
    }

    /**
     * Insert event row.
     *
     * @param EventCreatorData $event The event
     *
     * @return int The new ID
     */
    public function insertEvent(EventCreatorData $event): int
    {
        $row = [
            'client_name' => $event->clientName,
            'event_name' => $event->eventName,
            'event_date' => $event->eventDate,
        ];

        return (int)$this->queryFactory->newInsert(TableName::EVENTS, $row)->execute()->lastInsertId();
    }
}
