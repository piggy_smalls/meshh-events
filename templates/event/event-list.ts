import './event-list.css';

export class EventList {

    public constructor() {
        //($('#data-table') as any).DataTable();

        ($('#data-table') as any).DataTable({
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 1,2,3 ],
                    "orderable": false
                },
            ],
            'colReorder': true,
            'order': [[ 0, 'desc' ]],
            'processing': true,
            'serverSide': true,
            'language': {
                //  'url': __('js/datatable-english.json')
            },
            'ajax': {
                'url': 'event-list-json',
                'type': 'POST'
            },
            'columns': [
                {'data': 'uid'},
                {'data': 'client_name'},
                {'data': 'event_name'},
                {'data': 'event_date'}
            ],
        });

    };

    public demo() {
        return $('#testbutton').html();
    }
};

$(function () {
    new EventList();
});
